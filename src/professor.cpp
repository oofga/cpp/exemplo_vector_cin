#include "professor.hpp"
#include <iostream>

Professor::Professor(){
    set_nome("");
    set_telefone("000");
    set_cpf(0);
    set_salario(0.0);
    set_formacao("");
}
Professor::Professor(string nome, string telefone, long cpf, float salario, string formacao){
    set_nome(nome);
    set_telefone(telefone);
    set_cpf(cpf);
    set_salario(salario);
    set_formacao(formacao);
}
Professor::~Professor(){};


float Professor::get_salario(){
    return salario;
}
void Professor::set_salario(float salario){
    this->salario = salario;
}
string Professor::get_formacao(){
    return formacao;
}
void Professor::set_formacao(string formacao){
    this->formacao = formacao;
}
void Professor::imprime_dados(){
    cout << "Nome: " << get_nome() << endl;
    cout << "Telefone: " << get_telefone() << endl;
    cout << "CPF: " << get_cpf() << endl;
    cout << "Salário: " << get_salario() << endl;
    cout << "Formação: " << get_formacao() << endl;

}















